const Joi = require('joi')
const { createUserSchema, searchUserSchema } = require('../schemes/index')

exports.handleCreateUser = async ctx => {
    try {
        const validationResult = Joi.validate(ctx.request.body, createUserSchema)
        if (validationResult.error) {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: validationResult.error.message
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        } else {
            const { name, email } = ctx.request.body
            const { body } = await ctx.db.search({
                index: process.env.USERS_INDEX,
                body: {
                    query: {
                        match_phrase: { email: email }
                    }
                }
            })
            if (body.hits && body.hits.hits.length > 0) {
                const error = {
                    status: 409,
                    title: 'Conflict',
                    message: 'Email already registerd'
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            } else {
                const { body } = await ctx.db.index({
                    index: process.env.USERS_INDEX,
                    body: {
                        name: name,
                        email: email,
                        created_at: new Date().toISOString()
                    }
                })
                if (body && body.result === 'created') {
                    await ctx.db.indices.refresh({ index: process.env.USERS_INDEX })
                    const { body } = await ctx.db.search({
                        index: process.env.USERS_INDEX,
                        body: {
                            query: {
                                match_phrase: { email: email }
                            }
                        }
                    })
                    const newUser = {
                        id: body.hits.hits[0]['_id'],
                        name: body.hits.hits[0]['_source'].name,
                        email: body.hits.hits[0]['_source'].email,
                        created_at: body.hits.hits[0]['_source'].created_at
                    }
                    ctx.status = 201
                    ctx.body = {
                        data: newUser
                    }
                } else {
                    const error = {
                        status: 500,
                        title: 'Internal server error',
                        message: 'Can\'t index at this time'
                    }
                    ctx.status = error.status
                    ctx.body = {
                        error: error
                    }
                }
            }
        }
    } catch (err) {
        throw new Error(err.message)
    }
}
exports.handleSearchUser = async ctx => {
    try {
        const validationResult = Joi.validate(ctx.query, searchUserSchema)
        if (validationResult.error) {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: validationResult.error.message
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        } else {
            const { email } = ctx.query
            const { body } = await ctx.db.search({
                index: process.env.USERS_INDEX,
                body: {
                    query: {
                        match_phrase: { email: email }
                    }
                }
            })
            if (body.hits && body.hits.hits.length > 0) {
                const user = {
                    id: body.hits.hits[0]['_id'],
                    name: body.hits.hits[0]['_source'].name,
                    email: body.hits.hits[0]['_source'].email,
                    created_at: body.hits.hits[0]['_source'].created_at
                }
                ctx.status = 200
                ctx.body = {
                    data: user
                }
            } else {
                const error = {
                    status: 404,
                    title: 'Not found',
                    message: 'User not found'
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            }
        }
    } catch (err) {
        throw new Error(err.message)
    }
}
exports.handleUserDetails = async ctx => {
    try {
        if (ctx.params && ctx.params.id && ctx.params.id.length > 4) {
            const { id } = ctx.params
            const { body } = await ctx.db.search({
                index: process.env.USERS_INDEX,
                body: {
                    query: {
                        ids: {
                            values: [id]
                        }
                    }
                }
            })
            if (body.hits && body.hits.hits.length > 0) {
                const user = {
                    id: body.hits.hits[0]['_id'],
                    name: body.hits.hits[0]['_source'].name,
                    email: body.hits.hits[0]['_source'].email,
                    created_at: body.hits.hits[0]['_source'].created_at
                }
                ctx.status = 200
                ctx.body = {
                    data: user
                }
            } else {
                const error = {
                    status: 404,
                    title: 'Not found',
                    message: 'User not found'
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            }
        } else {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: 'ID is not valid'
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        }
    } catch (err) {
        throw new Error(err.message)
    }
}
