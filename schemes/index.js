const Joi = require('joi')

exports.createUserSchema = Joi.object().keys({
    name: Joi.string().min(4).lowercase().required(),
    email: Joi.string().email().required(),
})
exports.searchUserSchema = Joi.object().keys({
    email: Joi.string().email().required()
})