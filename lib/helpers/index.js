const users = require('../seeds/index')

// DB utilities
exports.bootstrapDB = async db => {
    // check if users exists
    let result = await db.indices.exists({
        index: process.env.USERS_INDEX
    })
    // if not, create users index
    if (result.statusCode === 404 && !result.body) {
        result = await db.indices.create({
            index: process.env.USERS_INDEX,
            body: {
                mappings: {
                    properties: {
                        name: { type: 'text' },
                        email: { type: 'text' },
                        created_at: { type: 'date' }
                    }
                }
            }
        })
        // halt the app if index creation fails
        if (result.statusCode !== 200 || !result.body.acknowledged) process.exit(1)
    }
    // seed DB 
    // TODO: replace with bulk API
    users.forEach(async user => {
        await db.index({
            index: process.env.USERS_INDEX,
            id: user.id,
            body: {
                name: user.name,
                email: user.email,
                created_at: user.create_at
            }
        })
    })
    await db.indices.refresh({ index: process.env.USERS_INDEX })
}
