const handler = require('../controllers/users')

module.exports = (router) => {
    const ENDPOINT = '/users'
    router
        .get(ENDPOINT, ctx => handler.handleSearchUser(ctx))
        .get(`${ENDPOINT}/:id`, ctx => handler.handleUserDetails(ctx))
        .post(ENDPOINT, ctx => handler.handleCreateUser(ctx))
}
