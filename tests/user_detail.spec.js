const chai = require('chai');
const chaiHttp = require('chai-http');
const uuidv1 = require('uuid/v1');
const {server} = require('../index');
const expect = chai.expect;
const {BASE_URL} = require('../constants/index');
const userSeeds = require('../lib/seeds/index');
const path = 'users';
chai.use(chaiHttp);

describe('Users APIs - UserDetail', () => {
  after(() => {
    server.close();
  });
  describe('When someone tries to get details of a user based on his ID',
      () => {
        it('It should succeed and send back the complete profile', (done) => {
          chai
              .request(server)
              .get(`${BASE_URL}/${path}/${userSeeds[0].id}`)
              .set('Content-Type', 'application/json')
              .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                expect(res.body).to.have.nested.property('data.id');
                expect(res.body).nested.property('data.id').eq(userSeeds[0].id);
                done();
              });
        });
      });
  describe('When someone tries to get details of a user with an invalid ID',
      () => {
        it('It should fail and send back 400 with apprpriate message',
            (done) => {
              chai
                  .request(server)
                  .get(`${BASE_URL}/${path}/${001}`)
                  .set('Content-Type', 'application/json')
                  .end((err, res) => {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an('object');
                    done();
                  });
            });
      });
  describe(`When someone tries to get details of a user with ID
  but it doesn\'t exist`,
  () => {
    it('It should fail and send back 404 with apprpriate message', (done) => {
      chai
          .request(server)
          .get(`${BASE_URL}/${path}/${uuidv1()}`)
          .set('Content-Type', 'application/json')
          .end((err, res) => {
            expect(res).to.have.status(404);
            expect(res.body).to.be.an('object');
            done();
          });
    });
  });
});
