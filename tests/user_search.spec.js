const chai = require('chai');
const chaiHttp = require('chai-http');
const uuidv1 = require('uuid/v1');
const {server} = require('../index');
const expect = chai.expect;
const {BASE_URL} = require('../constants/index');
const userSeeds = require('../lib/seeds/index');
const path = 'users';
chai.use(chaiHttp);

describe('Users APIs - UserSearch', () => {
  after(() => {
    server.close();
  });
  describe('When someone tries to search a user based on his email address',
      () => {
        it('It should succeed and send back the complete profile', (done) => {
          chai
              .request(server)
              .get(`${BASE_URL}/${path}`)
              .query({email: userSeeds[0].email})
              .set('Content-Type', 'application/json')
              .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                expect(res.body).to.have.nested.property('data.id');
                expect(res.body).nested.property('data.email')
                    .eq(userSeeds[0].email);
                done();
              });
        });
      });
  describe('When someone tries to search a user with an invalid email',
      () => {
        it('It should fail and send back 400 with apprpriate message',
            (done) => {
              chai
                  .request(server)
                  .get(`${BASE_URL}/${path}`)
                  .query({email: `invalid email`})
                  .set('Content-Type', 'application/json')
                  .end((err, res) => {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an('object');
                    done();
                  });
            });
      });
  describe(`When someone tries to search a user with email 
  but it doesn\'t exist`,
  () => {
    it('It should fail and send back 404 with apprpriate message',
        (done) => {
          chai
              .request(server)
              .get(`${BASE_URL}/${path}`)
              .query({email: `${uuidv1()}@test.test`})
              .set('Content-Type', 'application/json')
              .end((err, res) => {
                expect(res).to.have.status(404);
                expect(res.body).to.be.an('object');
                done();
              });
        });
  });
});
