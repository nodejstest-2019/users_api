const chai = require('chai');
const chaiHttp = require('chai-http');
const uuidv1 = require('uuid/v1');
const {server} = require('../index');
const expect = chai.expect;
const {BASE_URL} = require('../constants/index');
const userSeeds = require('../lib/seeds/index');
const path = 'users';
chai.use(chaiHttp);

describe('Users APIs - UserCreate', () => {
  after(() => {
    server.close();
  });
  describe('When a user tries to register with his name and email', () => {
    it('It should succeed and send back his complete profile', (done) => {
      chai
          .request(server)
          .post(`${BASE_URL}/${path}`)
          .set('Content-Type', 'application/json')
          .send({
            name: 'test name',
            email: `${uuidv1()}@test.test`,
          })
          .end((err, res) => {
            expect(res).to.have.status(201);
            expect(res.body).to.be.an('object');
            done();
          });
    });
  });
  describe('When a user tries to register with an invalid email', () => {
    it('It should throw a 400 Bad Request error for his email', (done) => {
      chai
          .request(server)
          .post(`${BASE_URL}/${path}`)
          .set('Content-Type', 'application/json')
          .send({
            name: userSeeds[0].name,
            email: 'invalid email',
          })
          .end((err, res) => {
            expect(res).to.have.status(400);
            expect(res.body).to.be.an('object');
            done();
          });
    });
  });
  describe('When an already registered user wants to register again', () => {
    it('It should throw a 409 conflict error for his email', (done) => {
      chai
          .request(server)
          .post(`${BASE_URL}/${path}`)
          .set('Content-Type', 'application/json')
          .send({
            name: userSeeds[0].name,
            email: userSeeds[0].email,
          })
          .end((err, res) => {
            expect(res).to.have.status(409);
            expect(res.body).to.be.an('object');
            done();
          });
    });
  });
});
